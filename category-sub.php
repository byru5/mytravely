
<?php include 'layout/head_src.php' ;?>
	

	<div class="container full">
    	<div class="top-nav border-bottom">
			<div class="row align-items-center">
				<div class="col">
					<a href="category.php"><img src="assets/images/svg/back-black.svg"></a>&nbsp;&nbsp;Petualangan
				</div>
			</div>
		</div>
        <div class="page-search">
            <div class="top-nav border-bottom">
                <div class="row align-items-center">
                    <div class="col"><input type="text" name="" autofocus="autofocus" class="form-search ml-3" placeholder="Apa yang anda cari?"></div>
                    <div class="col-auto text-right">
                        <img src="assets/images/svg/search-color.svg" id="open-search">
                    </div>
                </div>
            </div>
            
            <div class="desc">
                <a href="category-result.php">
                    <div class="row circle-list">
                        <div class="col-auto">
                            <div class="circle-img">
                                <img src="assets/images/uploaded/01.jpg">
                            </div>
                        </div>
                        <div class="col pl-0">
                            <p class="title text-black mb-0 mt-2">Rappelling (Turun tebing)</p>
                            <p class="p-desc">Eastice, Goldfort, Rosewall, Havenland, dll.</p>
                        </div>
                    </div>
                </a>
                <a href="category-result.php">
                    <div class="row circle-list">
                        <div class="col-auto">
                            <div class="circle-img">
                                <img src="assets/images/uploaded/01.jpg">
                            </div>
                        </div>
                        <div class="col pl-0">
                            <p class="title text-black mb-0 mt-2">Canyoning (Penelusuran Sungai)</p>
                            <p class="p-desc">Vertwynne, Valhurst, Woodview, Summerelf, dll.</p>
                        </div>
                    </div>
                </a>
                <a href="category-result.php">
                    <div class="row circle-list">
                        <div class="col-auto">
                            <div class="circle-img">
                                <img src="assets/images/uploaded/01.jpg">
                            </div>
                        </div>
                        <div class="col pl-0">
                            <p class="title text-black mb-0 mt-2">Climbing (Panjat Tebing)</p>
                            <p class="p-desc">Dago Dream Park, Curug Dago, dll.</p>
                        </div>
                    </div>
                </a>
                <a href="category-result.php">
                    <div class="row circle-list">
                        <div class="col-auto">
                            <div class="circle-img">
                                <img src="assets/images/uploaded/01.jpg">
                            </div>
                        </div>
                        <div class="col pl-0">
                            <p class="title text-black mb-0 mt-2">Hiking (Panjat Gunung)</p>
                            <p class="p-desc">Eastice, Goldfort, Rosewall, Havenland, dll</p>
                        </div>
                    </div>
                </a>
            </div>      
        </div>
    </div>


	<div class="wrap-nav"></div>
	<div class="full-nav bottom">
		<div class="container">
			<div class="bottom-nav">
				<div class="row">
					<div class="col text-center">
						<a href="index.php">
							<div class="img-home">
								<img src="assets/images/svg/menu-home-no.svg">
							</div>
							<p>beranda</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="category.php" class="active">
							<div class="img-category">
								<img src="assets/images/svg/menu-category-no.svg">
							</div>
							<p>kategori</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="nearest-gps.php">
							<div class="img-near">
								<img src="assets/images/svg/menu-near-no.svg">
							</div>
							<p>tedekat</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="popular.php">
							<div class="img-popular">
								<img src="assets/images/svg/menu-popular-no.svg">
							</div>
							<p>populer</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
		
		
<?php include 'layout/footer.php' ;?>

	
