
<?php include 'layout/head_src.php' ;?>
	
	<div class="container">
		<div class="top-nav">
			<div class="row align-items-center">
				<div class="col">
					<a href="index-detail.php"><img src="assets/images/svg/back-black.svg"></a>
					<span>Ulasan (320)</span>
				</div>
			</div>
		</div>
		<div class="detail pb-5">
			<div class="desc content">
				<div class="row list-review">
					<div class="col">
						<p class="title">Sherina</p>
						<p class="sub-title text-secondary">20 Januari 2018</p>
					</div>
					<div class="col">
						<p class="stars text-right text-warning">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
							<i class="fa fa-star-o"></i>
						</p>
						<p class="sub-title text-right text-secondary">
							Tripadvisor
						</p>
					</div>
					<div class="col-sm-12 mt-2">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						</p>
					</div>
				</div>
				<hr class="divider">

				<div class="row list-review">
					<div class="col">
						<p class="title">Sherina</p>
						<p class="sub-title text-secondary">20 Januari 2018</p>
					</div>
					<div class="col">
						<p class="stars text-right text-warning">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
							<i class="fa fa-star-o"></i>
						</p>
						<p class="sub-title text-right text-secondary">
							Tripadvisor
						</p>
					</div>
					<div class="col-sm-12 mt-2">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						</p>
					</div>
				</div>
				<hr class="divider">

				<div class="row list-review">
					<div class="col">
						<p class="title">Sherina</p>
						<p class="sub-title text-secondary">20 Januari 2018</p>
					</div>
					<div class="col">
						<p class="stars text-right text-warning">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
							<i class="fa fa-star-o"></i>
						</p>
						<p class="sub-title text-right text-secondary">
							Tripadvisor
						</p>
					</div>
					<div class="col-sm-12 mt-2">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						</p>
					</div>
				</div>
				<hr class="divider">

				<div class="row list-review">
					<div class="col">
						<p class="title">Sherina</p>
						<p class="sub-title text-secondary">20 Januari 2018</p>
					</div>
					<div class="col">
						<p class="stars text-right text-warning">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
							<i class="fa fa-star-o"></i>
						</p>
						<p class="sub-title text-right text-secondary">
							Tripadvisor
						</p>
					</div>
					<div class="col-sm-12 mt-2">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						</p>
					</div>
				</div>
				<hr class="divider">

				<div class="row list-review">
					<div class="col">
						<p class="title">Sherina</p>
						<p class="sub-title text-secondary">20 Januari 2018</p>
					</div>
					<div class="col">
						<p class="stars text-right text-warning">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
							<i class="fa fa-star-o"></i>
						</p>
						<p class="sub-title text-right text-secondary">
							Tripadvisor
						</p>
					</div>
					<div class="col-sm-12 mt-2">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						</p>
					</div>
				</div>
				<hr class="divider">

				<div class="row list-review">
					<div class="col">
						<p class="title">Sherina</p>
						<p class="sub-title text-secondary">20 Januari 2018</p>
					</div>
					<div class="col">
						<p class="stars text-right text-warning">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
							<i class="fa fa-star-o"></i>
						</p>
						<p class="sub-title text-right text-secondary">
							Tripadvisor
						</p>
					</div>
					<div class="col-sm-12 mt-2">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						</p>
					</div>
				</div>
				<hr class="divider">

				<div class="row list-review">
					<div class="col">
						<p class="title">Sherina</p>
						<p class="sub-title text-secondary">20 Januari 2018</p>
					</div>
					<div class="col">
						<p class="stars text-right text-warning">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
							<i class="fa fa-star-o"></i>
						</p>
						<p class="sub-title text-right text-secondary">
							Tripadvisor
						</p>
					</div>
					<div class="col-sm-12 mt-2">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						</p>
					</div>
				</div>


			</div>

		</div>
		<div class="stack-page">
			<p>Anda telah mencapai batas halaman terakhir</p>
			<a href="#" class="btn btn-outline-secondary rounded">&nbsp;&nbsp;<i class="fa fa-arrow-up"></i>&nbsp;&nbsp;Kembali ke Atas&nbsp;&nbsp;</a>
		</div>
	</div>
	
	
<?php include 'layout/footer.php' ;?>



	
