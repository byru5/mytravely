
<?php include 'layout/head_src.php' ;?>
<?php include 'layout/top_nav.php' ;?>
	
	<div class="wrap-nav mb-3"></div>

	<div class="container">
		<div class="card">
			<div class="badges red">populer</div>
			<div class="card-1">
				<div class="square">
					<div class="images">
						<a class="fancybox" href="assets/images/uploaded/02.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
							<img class="object-fit_cover" src="assets/images/uploaded/02.jpg" alt="">
						</a>
					</div>
				</div>
			</div>
			<a href="index-detail.php" >
				<div class="desc">
					<div class="row">
						<div class="col">
							<p class="title text-black text-black">Wisata Gunung Bromo</p>
							<p class="sub-title text-secondary">Malang, Jawa Timur</p>
						</div>
						<div class="col-auto">
							<p class="stars text-right text-warning">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-o"></i>
								&nbsp;8.5
							</p>
							<p class="sub-title text-right text-secondary">
								355 ulasan
							</p>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="card">
			<div class="badges purple">terdekat</div>
			<div class="card-1">
				<div class="owl-carousel owl-theme" id="slide_1">
					<a class="fancybox" href="assets/images/uploaded/02.jpg" data-fancybox-group="gallery_3" title="Lorem ipsum dolor sit amet1">
						<div class="square item" style="background: url('assets/images/uploaded/02.jpg') center center;"></div>
					</a>
					<a class="fancybox" href="assets/images/uploaded/01.jpg" data-fancybox-group="gallery_3" title="Lorem ipsum dolor sit amet1">
						<div class="square item" style="background: url('assets/images/uploaded/01.jpg') center center;"></div>
					</a>
					<a class="fancybox" href="assets/images/uploaded/01.jpg" data-fancybox-group="gallery_3" title="Lorem ipsum dolor sit amet1">
						<div class="square item" style="background: url('assets/images/uploaded/01.jpg') center center;"></div>
					</a>
					<div class="square item-video" style="background: url('assets/images/uploaded/02.jpg') center center;">
							<a class="fancybox fancybox.iframe btn-play" href="https://www.youtube.com/embed/iYbSMT8DweU?autoplay=1" allow="autoplay; encrypted-media" target="_top" data-fancybox-group="gallery_3">
								<div class=""><i class="fa fa-play-circle fa-5x"></i></div>
							</a>
					</div>
				</div>
				<div class="owl-theme">
					<div class="owl-nav">
						<div class="container-dots" id="dots_1"></div>
					</div>
				</div>
			</div>
			<a href="index-detail.php">
				<div class="desc">
					<div class="row">
						<div class="col">
							<p class="title text-black">Wisata Gunung Bromo Lorem ipsum dolor sit amet, consectetur adip isicing elit</p>
							<p class="sub-title text-secondary">Malang, Jawa Timur</p>
						</div>
						<div class="col-auto">
							<p class="stars text-right text-warning">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-o"></i>
								&nbsp;8.5
							</p>
							<p class="sub-title text-right text-secondary">
								355 ulasan
							</p>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="card">
			<div class="badges green">rekomendasi</div>
			<div class="card-1">
				<div class="square">
					<div class="images owl-carousel owl-theme" id="slide_2">
						<div class="item-video ">
							<a class="fancybox fancybox.iframe btn-play" href="https://www.youtube.com/embed/iYbSMT8DweU?autoplay=1" allow="autoplay; encrypted-media" target="_top" data-fancybox-group="gallery_2"><div class=""><i class="fa fa-play-circle fa-5x"></i></div>
							</a>
							<img class="object-fit_cover" src="assets/images/uploaded/01.jpg" alt="">
						</div>
						<div class="item">
							<a class="fancybox" href="assets/images/uploaded/02.jpg" data-fancybox-group="gallery_2" title="Lorem ipsum dolor sit amet1">
								<img class="object-fit_cover" src="assets/images/uploaded/02.jpg" alt="">
							</a>
						</div>
						<div class="item">
							<a class="fancybox" href="assets/images/uploaded/01.jpg" data-fancybox-group="gallery_2" title="Lorem ipsum dolor sit amet2">
								<img class="object-fit_cover" src="assets/images/uploaded/01.jpg" alt="">
							</a>
						</div>
						<div class="item">
							<a class="fancybox" href="assets/images/uploaded/01.jpg" data-fancybox-group="gallery_2" title="Lorem ipsum dolor sit amet3">
								<img class="object-fit_cover" src="assets/images/uploaded/01.jpg" alt="">
							</a>
						</div>
						<div class="item">
							<a class="fancybox" href="assets/images/uploaded/01.jpg" data-fancybox-group="gallery_2" title="Lorem ipsum dolor sit amet4">
								<img class="object-fit_cover" src="assets/images/uploaded/01.jpg" alt="">
							</a>
						</div>
					</div>
				</div>
				<div class="owl-theme">
					<div class="owl-nav">
						<div class="container-dots" id="dots_2"></div>
					</div>
				</div>
			</div>
			<a href="index-detail.php">
				<div class="desc">
					<div class="row">
						<div class="col">
							<p class="title text-black">Wisata Gunung Bromo</p>
							<p class="sub-title text-secondary">Malang, Jawa Timur</p>
						</div>
						<div class="col-auto">
							<p class="stars text-right text-warning">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-o"></i>
								&nbsp;8.5
							</p>
							<p class="sub-title text-right text-secondary">
								355 ulasan
							</p>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="card">
			<div class="badges red">populer</div>
			<div class="card-1">
				<div class="square">
					<div class="images">
						<a class="fancybox" href="assets/images/uploaded/01.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
							<img class="object-fit_cover" src="assets/images/uploaded/01.jpg" alt="">
						</a>
					</div>
				</div>
				<div class="owl-theme">
					<div class="owl-nav">
						<div class="container-dots"></div>
					</div>
				</div>
			</div>
			<a href="index-detail.php">
				<div class="desc">
					<div class="row">
						<div class="col">
							<p class="title text-black">Wisata Gunung Bromo Lorem ipsum dolor sit amet, consectetur adip isicing elit</p>
							<p class="sub-title text-secondary">Malang, Jawa Timur</p>
						</div>
						<div class="col-auto">
							<p class="stars text-right text-warning">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-o"></i>
								&nbsp;8.5
							</p>
							<p class="sub-title text-right text-secondary">
								355 ulasan
							</p>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="card">
			<div class="card-1">
				<div class="placement p_1" ></div>
			</div>
			
			<div class="desc">
				<div class="row">
					<div class="col">
						<div class="placement p_2"></div>
						<div class="placement p_3"></div>
					</div>
					<div class="col-auto">
						<div class="placement p_4"></div>
						<div class="placement p_5"></div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<!-- <div class="fr-spinner">
		<div class="sk-fading-circle">
		  <div class="sk-circle1 sk-circle"></div>
		  <div class="sk-circle2 sk-circle"></div>
		  <div class="sk-circle3 sk-circle"></div>
		  <div class="sk-circle4 sk-circle"></div>
		  <div class="sk-circle5 sk-circle"></div>
		  <div class="sk-circle6 sk-circle"></div>
		  <div class="sk-circle7 sk-circle"></div>
		  <div class="sk-circle8 sk-circle"></div>
		  <div class="sk-circle9 sk-circle"></div>
		  <div class="sk-circle10 sk-circle"></div>
		  <div class="sk-circle11 sk-circle"></div>
		  <div class="sk-circle12 sk-circle"></div>
		</div>
	</div> -->


	<div class="wrap-nav"></div>
	<div class="full-nav bottom" id="id-bottom-nav">
		<div class="container">
			<div class="bottom-nav">
				<div class="row">
					<div class="col text-center">
						<a href="index.php" class="active">
							<div class="img-home">
								<img src="assets/images/svg/menu-home-no.svg">
							</div>
							<p>beranda</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="category.php">
							<div class="img-category">
								<img src="assets/images/svg/menu-category-no.svg">
							</div>
							<p>kategori</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="nearest-gps.php">
							<div class="img-near">
								<img src="assets/images/svg/menu-near-no.svg">
							</div>
							<p>tedekat</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="popular.php">
							<div class="img-popular">
								<img src="assets/images/svg/menu-popular-no.svg">
							</div>
							<p>populer</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	<script type="text/javascript">
        var prevScrollpos = window.pageYOffset;
        window.onscroll = function() {
            var currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
                document.getElementById("id-top-nav").style.top = "0px";
                // document.getElementById("id-bottom-nav").style.bottom = "0px";
            } else {
                document.getElementById("id-top-nav").style.top = "-50px";
                // document.getElementById("id-bottom-nav").style.bottom = "-50px";
            }
            prevScrollpos = currentScrollPos;
        }
    </script>
		
<?php include 'layout/footer.php' ;?>

	
