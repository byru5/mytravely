
<?php include 'layout/head_src.php' ;?>
	<div class="full-nav mb-3" id="id-top-nav">
		<div class="container">
			<div class="top-nav" >
				<div class="row align-items-center">
					<div class="col">
						<a href="index-detail.php"><img src="assets/images/svg/back-black.svg"></a>&nbsp;&nbsp;Hotels (128)
					</div>
					<div class="col-auto text-right">
						<span id="open-search"><img src="assets/images/svg/search-color.svg"></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="wrap-nav"></div>
	<div class="container">	
		<?php include 'layout/filter.php' ?>
		<div class="wrap-nav"></div>
		<div class="card">
			<div class="desc">
				<div class="row">
					<div class="col-auto">
						<div class="thumbnail">
							<img src="assets/images/uploaded/01.jpg">
						</div>
					</div>
					<div class="col-auto attb-hotel">
						<p class="mt-0">Crowne Plaza Bandung</p>
						<p class="stars text-warning">
							<span class="badge badge-success">7.8</span>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
						</p>
						<p><img src="assets/images/svg/pin-map.svg"><span class="text-gray text-sub">&nbsp;&nbsp;0,3 km dari lokasi</span></p>
						<a href="" class="btn btn-outline-primary btn-sm">Lihat Detail</a>
					</div>
				</div>
				<hr class="divider mb-0">
				<div class="row align-items-center">
					<div class="col-auto">
						<p class="text-gray mt-0"><small>Hotels.com</small></p>
						<p>
							<span class="badge badge-danger">-30%</span>&nbsp;&nbsp;
							<span class="text-red"><del>Rp 414.600</del></span>&nbsp;&nbsp;
							<span class="text-green">Rp 289.400</span>
						</p>
					</div>
					<div class="col">
						<p>&nbsp;</p>
						<a href="" class="btn btn-success pull-right">Pilih</a>
					</div>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="desc">
				<div class="row">
					<div class="col-auto">
						<div class="thumbnail">
							<img src="assets/images/uploaded/01.jpg">
						</div>
					</div>
					<div class="col-auto attb-hotel">
						<p class="mt-0">Crowne Plaza Bandung</p>
						<p class="stars text-warning">
							<span class="badge badge-success">7.8</span>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
						</p>
						<p><img src="assets/images/svg/pin-map.svg"><span class="text-gray text-sub">&nbsp;&nbsp;0,3 km dari lokasi</span></p>
						<a href="" class="btn btn-outline-primary btn-sm">Lihat Detail</a>
					</div>
				</div>
				<hr class="divider mb-0">
				<div class="row align-items-center">
					<div class="col-auto">
						<p class="text-gray mt-0"><small>Hotels.com</small></p>
						<p>
							<span class="badge badge-danger">-30%</span>&nbsp;&nbsp;
							<span class="text-red"><del>Rp 414.600</del></span>&nbsp;&nbsp;
							<span class="text-green">Rp 289.400</span>
						</p>
					</div>
					<div class="col">
						<p>&nbsp;</p>
						<a href="" class="btn btn-success pull-right">Pilih</a>
					</div>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="desc">
				<div class="row">
					<div class="col-auto">
						<div class="thumbnail">
							<img src="assets/images/uploaded/01.jpg">
						</div>
					</div>
					<div class="col-auto attb-hotel">
						<p class="mt-0">Crowne Plaza Bandung</p>
						<p class="stars text-warning">
							<span class="badge badge-success">7.8</span>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
						</p>
						<p><img src="assets/images/svg/pin-map.svg"><span class="text-gray text-sub">&nbsp;&nbsp;0,3 km dari lokasi</span></p>
						<a href="" class="btn btn-outline-primary btn-sm">Lihat Detail</a>
					</div>
				</div>
				<hr class="divider mb-0">
				<div class="row align-items-center">
					<div class="col-auto">
						<p class="text-gray mt-0"><small>Hotels.com</small></p>
						<p>
							<span class="badge badge-danger">-30%</span>&nbsp;&nbsp;
							<span class="text-red"><del>Rp 414.600</del></span>&nbsp;&nbsp;
							<span class="text-green">Rp 289.400</span>
						</p>
					</div>
					<div class="col">
						<p>&nbsp;</p>
						<a href="" class="btn btn-success pull-right">Pilih</a>
					</div>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="desc">
				<div class="row">
					<div class="col-auto">
						<div class="thumbnail">
							<img src="assets/images/uploaded/01.jpg">
						</div>
					</div>
					<div class="col-auto attb-hotel">
						<p class="mt-0">Crowne Plaza Bandung</p>
						<p class="stars text-warning">
							<span class="badge badge-success">7.8</span>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
						</p>
						<p><img src="assets/images/svg/pin-map.svg"><span class="text-gray text-sub">&nbsp;&nbsp;0,3 km dari lokasi</span></p>
						<a href="" class="btn btn-outline-primary btn-sm">Lihat Detail</a>
					</div>
				</div>
				<hr class="divider mb-0">
				<div class="row align-items-center">
					<div class="col-auto">
						<p class="text-gray mt-0"><small>Hotels.com</small></p>
						<p>
							<span class="badge badge-danger">-30%</span>&nbsp;&nbsp;
							<span class="text-red"><del>Rp 414.600</del></span>&nbsp;&nbsp;
							<span class="text-green">Rp 289.400</span>
						</p>
					</div>
					<div class="col">
						<p>&nbsp;</p>
						<a href="" class="btn btn-success pull-right">Pilih</a>
					</div>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="desc">
				<div class="row">
					<div class="col-auto">
						<div class="thumbnail">
							<img src="assets/images/uploaded/01.jpg">
						</div>
					</div>
					<div class="col-auto attb-hotel">
						<p class="mt-0">Crowne Plaza Bandung</p>
						<p class="stars text-warning">
							<span class="badge badge-success">7.8</span>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
						</p>
						<p><img src="assets/images/svg/pin-map.svg"><span class="text-gray text-sub">&nbsp;&nbsp;0,3 km dari lokasi</span></p>
						<a href="" class="btn btn-outline-primary btn-sm">Lihat Detail</a>
					</div>
				</div>
				<hr class="divider mb-0">
				<div class="row align-items-center">
					<div class="col-auto">
						<p class="text-gray mt-0"><small>Hotels.com</small></p>
						<p>
							<span class="badge badge-danger">-30%</span>&nbsp;&nbsp;
							<span class="text-red"><del>Rp 414.600</del></span>&nbsp;&nbsp;
							<span class="text-green">Rp 289.400</span>
						</p>
					</div>
					<div class="col">
						<p>&nbsp;</p>
						<a href="" class="btn btn-success pull-right">Pilih</a>
					</div>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="desc">
				<div class="row">
					<div class="col-auto">
						<div class="thumbnail">
							<img src="assets/images/uploaded/01.jpg">
						</div>
					</div>
					<div class="col-auto attb-hotel">
						<p class="mt-0">Crowne Plaza Bandung</p>
						<p class="stars text-warning">
							<span class="badge badge-success">7.8</span>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star-half-o"></i>
						</p>
						<p><img src="assets/images/svg/pin-map.svg"><span class="text-gray text-sub">&nbsp;&nbsp;0,3 km dari lokasi</span></p>
						<a href="" class="btn btn-outline-primary btn-sm">Lihat Detail</a>
					</div>
				</div>
				<hr class="divider mb-0">
				<div class="row align-items-center">
					<div class="col-auto">
						<p class="text-gray mt-0"><small>Hotels.com</small></p>
						<p>
							<span class="badge badge-danger">-30%</span>&nbsp;&nbsp;
							<span class="text-red"><del>Rp 414.600</del></span>&nbsp;&nbsp;
							<span class="text-green">Rp 289.400</span>
						</p>
					</div>
					<div class="col">
						<p>&nbsp;</p>
						<a href="" class="btn btn-success pull-right">Pilih</a>
					</div>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="desc">
				<div class="row">
					<div class="col-auto">
						<div class="thumbnail">
							<div class="placement p_1 "></div>
						</div>
					</div>
					<div class="col pt-2">
						<div class="placement p_2"></div>
						<div class="placement p_6"></div>
						<div class="placement p_6"></div>
					</div>
				</div>
				<hr class="divider">
				<div class="row ">
					<div class="col-7">
						<div class="placement p_7"></div>
					</div>
					<div class="col-5">
						<div class="placement p_btn"></div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="stack-page">
			<p>Anda telah mencapai batas halaman terakhir</p>
			<a href="#" class="btn btn-outline-secondary rounded">&nbsp;&nbsp;<i class="fa fa-arrow-up"></i>&nbsp;&nbsp;Kembali ke Atas&nbsp;&nbsp;</a>
		</div>

	</div>
	<script type="text/javascript">
        var prevScrollpos = window.pageYOffset;
        window.onscroll = function() {
            var currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
                document.getElementById("id-top-nav").style.top = "0px";
                // document.getElementById("id-bottom-nav").style.bottom = "0px";
                document.getElementById("id-filter-nav").style.top = "45px";
                document.getElementById("id-sortby").style.top = "90px";
                document.getElementById("id-filter").style.top = "90px";
            } else {
                document.getElementById("id-top-nav").style.top = "-50px";
                // document.getElementById("id-bottom-nav").style.bottom = "-50px";
                document.getElementById("id-filter-nav").style.top = "0px";
                document.getElementById("id-sortby").style.top = "45px";
                document.getElementById("id-filter").style.top = "45px";
            }
            prevScrollpos = currentScrollPos;
        }
    </script>
	
	
<?php include 'layout/footer.php' ;?>

	
