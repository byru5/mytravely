
<?php include 'layout/head_src.php' ;?>
	
	<div class="container">
		<div class="top-nav float">
			<div class="row align-items-center">
				<div class="col">
					<a href="index.php" id="link"><img src="assets/images/svg/back-white.svg" ></a>&nbsp;&nbsp;<span>Wisata Gunung Bromo</span>
				</div>
				<div class="col text-right">
					<img src="assets/images/svg/share-white.svg" id="btn-share">
				</div>
			</div>
		</div>
		<div class="detail pb-5">
			<div class="card-1">
				<div class="owl-carousel owl-theme" id="slide_1">
					<a class="fancybox" href="assets/images/uploaded/02.jpg" data-fancybox-group="gallery_3" title="Lorem ipsum dolor sit amet1">
						<div class="square item" style="background: url('assets/images/uploaded/02.jpg') center center;"></div>
					</a>
					<a class="fancybox" href="assets/images/uploaded/01.jpg" data-fancybox-group="gallery_3" title="Lorem ipsum dolor sit amet1">
						<div class="square item" style="background: url('assets/images/uploaded/01.jpg') center center;"></div>
					</a>
					<a class="fancybox" href="assets/images/uploaded/01.jpg" data-fancybox-group="gallery_3" title="Lorem ipsum dolor sit amet1">
						<div class="square item" style="background: url('assets/images/uploaded/01.jpg') center center;"></div>
					</a>
					<div class="square item-video" style="background: url('assets/images/uploaded/02.jpg') center center;">
							<a class="fancybox fancybox.iframe btn-play" href="https://www.youtube.com/embed/iYbSMT8DweU?autoplay=1" allow="autoplay; encrypted-media" target="_top" data-fancybox-group="gallery_3">
								<div class=""><i class="fa fa-play-circle fa-5x"></i></div>
							</a>
					</div>
				</div>
				<div class="owl-theme">
					<div class="owl-nav">
						<div class="container-dots" id="dots_1"></div>
					</div>
				</div>
			</div>

			<div class="desc">
				<p class="title">Wisata Gunung Bromo</p>
				<p class="sub-title text-secondary">Malang, Jawa Timur</p>

				<div class="row mt-3">
					<div class="col">
						<p class="title">Penilaian</p>
						<h3 class="text-green">8.5</h3>
					</div>
					<div class="col">
						<p class="sub-title text-right text-secondary">
							by Tripadvisor
						</p>
						<p class="stars text-right text-warning">
							<i class="fa fa-star fa-15x"></i>
							<i class="fa fa-star fa-15x"></i>
							<i class="fa fa-star fa-15x"></i>
							<i class="fa fa-star fa-15x"></i>
							<i class="fa fa-star-half-o fa-15x"></i>
						</p>
						
					</div>
				</div>

				<div class="">
					<div class="tab row">
						<div class="col active" id="tab-1">Deskripsi</div>
						<div class="col" id="tab-2">Ulasan (322)</div>
					</div>
					
					<div class="content pb-5" id="content-tab-1">
						<p class="text-gray">Deskripsi</p>
						<p>Taman hiburan keluarga indoor dengan wahana seru, bioskop, & area bermain untuk anak usia dini.</p>
						<br>
						<p class="text-gray">Alamat</p>
						<p>Jl. Gatot Subroto No.289, Cibangkong, Batununggal, Kota Bandung, Jawa Barat 40273</p>
						<br>
						<p class="text-gray">Provinsi</p>
						<p>Jawa Barat</p>
						<br>
						<p class="text-gray">Jam Buka</p>
						<p>08 : 00 - 21:00 WIB</p>
						<br>
						<p class="text-gray">Telepon</p>
						<p>(022) 86012555</p>
					</div>

					<div class="content pb-5" id="content-tab-2" style="display: none">
						<div class="row list-review">
							<div class="col">
								<p class="title">Sherina</p>
								<p class="sub-title text-secondary">20 Januari 2018</p>
							</div>
							<div class="col">
								<p class="stars text-right text-warning">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
									<i class="fa fa-star-o"></i>
								</p>
								<p class="sub-title text-right text-secondary">
									Tripadvisor
								</p>
							</div>
							<div class="col-sm-12 mt-2">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								</p>
							</div>
						</div>
						<hr class="divider">

						<div class="row list-review">
							<div class="col">
								<p class="title">Sherina</p>
								<p class="sub-title text-secondary">20 Januari 2018</p>
							</div>
							<div class="col">
								<p class="stars text-right text-warning">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
									<i class="fa fa-star-o"></i>
								</p>
								<p class="sub-title text-right text-secondary">
									Tripadvisor
								</p>
							</div>
							<div class="col-sm-12 mt-2">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								</p>
							</div>
						</div>
						<hr class="divider">

						<div class="row list-review">
							<div class="col">
								<p class="title">Sherina</p>
								<p class="sub-title text-secondary">20 Januari 2018</p>
							</div>
							<div class="col">
								<p class="stars text-right text-warning">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
									<i class="fa fa-star-o"></i>
								</p>
								<p class="sub-title text-right text-secondary">
									Tripadvisor
								</p>
							</div>
							<div class="col-sm-12 mt-2">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								</p>
							</div>
						</div>
						<hr class="divider">

						<div class="row list-review">
							<div class="col">
								<p class="title">Sherina</p>
								<p class="sub-title text-secondary">20 Januari 2018</p>
							</div>
							<div class="col">
								<p class="stars text-right text-warning">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
									<i class="fa fa-star-o"></i>
								</p>
								<p class="sub-title text-right text-secondary">
									Tripadvisor
								</p>
							</div>
							<div class="col-sm-12 mt-2">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								</p>
							</div>
						</div>
						<hr class="divider">
						<div class="row list-review">
							<div class="col">
								<p class="title">Sherina</p>
								<p class="sub-title text-secondary">20 Januari 2018</p>
							</div>
							<div class="col">
								<p class="stars text-right text-warning">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
									<i class="fa fa-star-o"></i>
								</p>
								<p class="sub-title text-right text-secondary">
									Tripadvisor
								</p>
							</div>
							<div class="col-sm-12 mt-2">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								</p>
							</div>
						</div>
						<hr class="divider">

						<div class="row list-review">
							<div class="col">
								<p class="title">Sherina</p>
								<p class="sub-title text-secondary">20 Januari 2018</p>
							</div>
							<div class="col">
								<p class="stars text-right text-warning">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
									<i class="fa fa-star-o"></i>
								</p>
								<p class="sub-title text-right text-secondary">
									Tripadvisor
								</p>
							</div>
							<div class="col-sm-12 mt-2">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								</p>
							</div>
						</div>
						<hr class="divider">

						<div class="row list-review">
							<div class="col">
								<p class="title">Sherina</p>
								<p class="sub-title text-secondary">20 Januari 2018</p>
							</div>
							<div class="col">
								<p class="stars text-right text-warning">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
									<i class="fa fa-star-o"></i>
								</p>
								<p class="sub-title text-right text-secondary">
									Tripadvisor
								</p>
							</div>
							<div class="col-sm-12 mt-2">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
								</p>
							</div>
						</div>
						<br>
						<br>
						<a href="index-detail-ulasan.php" class="btn btn-outline-primary btn-block">Lihat semua ulasan</a>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="container">
		<a href="list-hotel.php">
			<div class="btn btn-lg btn-float">
				<i class="fa fa-pencil"></i>&nbsp;&nbsp;PESAN HOTEL TERDEKAT
			</div>
		</a>
	</div>
	<div class="container">
		<div class="container-share" style="display: none">
			<div class="container-icon desc">
				<p>SHARE VIA :</p>
				<div class="row no-gutters mb-3">
					<div class="col">
						<a href="#">
							<div class="share-icon">
								<i class="fa fa-facebook"></i>
							</div>
							<p class="text-center text-black">Facebook</p>
						</a>
					</div>
					<div class="col">
						<a href="#">
							<div class="share-icon">
								<i class="fa fa-twitter text-info"></i>
							</div>
							<p class="text-center text-black">Twitter</p>
						</a>
					</div>
					<div class="col">
						<a href="#">
							<div class="share-icon text-green">
								<i class="fa fa-whatsapp"></i>
							</div>
							<p class="text-center text-black">Whatsapp</p>
						</a>
					</div>
					<div class="col">
						<a href="#">
							<div class="share-icon" id="copy_1" data-clipboard-text="Tercopy Gan!">
								<div class="copied" style="display: none;">Copied!<div class="arrow-down"></div></div>
								<i class="fa fa-link text-red"></i>
							</div>
							<p class="text-center text-black" >Copy Link</p>
						</a>
					</div>
				</div>
			</div>
			<div class="wrapper"></div>
		</div>
		
	</div>

	
<?php include 'layout/footer.php' ;?>



	
