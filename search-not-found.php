
<?php include 'layout/head_src.php' ;?>
<?php include 'layout/top_nav.php' ;?>
	
	<div class="container full">
		<div class="desc">
			<div class="vertical-normal break text-center">
				<div class="middle">
					<img src="assets/images/svg/search_not_found.svg">
					<h1 class="text-red mt-3">Hmm..</h1>
					<h2>Pencarian tidak ditemukan.</h2>
					<p class="text-gray">Silahkah periksa kembali kata kunci<br> yang anda masukkan</p>
					<a href="index.php" class="btn btn-success btn-lg mt-3"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Masukkan Kata Kunci</a>
				</div>
				
			</div>
			<br>
			<p class="mt-5">Paling banyak dicari saat ini</p>
		</div>
		
		<hr class="divider my-0">
		<div class="desc pb-5">
			<a href="">
				<div class="row circle-list">
					<div class="col-auto">
						<div class="circle-img">
							<img src="assets/images/uploaded/01.jpg">
						</div>
					</div>
					<div class="col px-0">
						<p class="title text-black mb-0	mt-2">Dago, Bandung</p>
						<p class="p-desc">Dago Dream Park, Curug Dago, Tebing Keraton, dll as asd as</p>
					</div>
					<div class="col-auto">
						<div class="btn btn-bagdes mt-3">1</div>
					</div>
				</div>
			</a>
			<a href="">
				<div class="row circle-list">
					<div class="col-auto">
						<div class="circle-img">
							<img src="assets/images/uploaded/01.jpg">
						</div>
					</div>
					<div class="col px-0">
						<p class="title text-black mb-0	mt-2">Puncak Bogor</p>
						<p class="p-desc">Little Venice Puncak, Taman Wisata Alam Gunung Pancar, Gunung Batu Jonggol, dll</p>
					</div>
					<div class="col-auto">
						<div class="btn btn-bagdes mt-3">99+</div>
					</div>
				</div>
			</a>
			<a href="">
				<div class="row circle-list">
					<div class="col-auto">
						<div class="circle-img">
							<img src="assets/images/uploaded/01.jpg">
						</div>
					</div>
					<div class="col px-0">
						<p class="title text-black mb-0	mt-2">Dago, Bandung</p>
						<p class="p-desc">Dago Dream Park, Curug Dago, Tebing Keraton, dll as asd as</p>
					</div>
					<div class="col-auto">
						<div class="btn btn-bagdes mt-3">58</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="full-nav bottom">
		<div class="container">
			<div class="bottom-nav">
				<div class="row">
					<div class="col text-center">
						<a href="index.php">
							<div class="img-home">
								<img src="assets/images/svg/menu-home-no.svg">
							</div>
							<p>beranda</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="category.php">
							<div class="img-category">
								<img src="assets/images/svg/menu-category-no.svg">
							</div>
							<p>kategori</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="nearest-gps.php" class="active">
							<div class="img-near">
								<img src="assets/images/svg/menu-near-no.svg">
							</div>
							<p>tedekat</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="popular.php">
							<div class="img-popular">
								<img src="assets/images/svg/menu-popular-no.svg">
							</div>
							<p>populer</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
		
		
<?php include 'layout/footer.php' ;?>

	
