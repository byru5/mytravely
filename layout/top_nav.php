	<div class="full-nav mb-3" id="id-top-nav">
		<div class="container">
			<div class="top-nav">
				<div class="row align-items-center">
					<div class="col">
						<a href="#"><img src="assets/images/svg/mytravely-logo.svg" class="logo"></a>
					</div>
					<div class="col text-center">
						<a href="#"><img src="assets/images/svg/mytravely-text.svg" class="logo-text" ></a>
					</div>
					<div class="col text-right" >
						<img src="assets/images/svg/search-color.svg" id="open-search">
					</div>
				</div>
			</div>
		</div>
	</div>
	