		
		<div class="filter-nav" id="id-filter-nav">
			<div class="row align-items-center">
				<div class="col col-sortby">
					<div class="filter" id="btn-sortby">
						<span>Terdekat</span>
						<img src="assets/images/svg/filter-chevron.svg" class=" pull-right">
					</div>
				</div>
				<div class="col col-filter text-center">
					<div class="filter" id="btn-filter">
						<img src="assets/images/svg/filter-setting.svg" class="">
						&nbsp;&nbsp;<span>Filter</span>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="side-sortby" style="display: none" id="id-sortby">
			<div class="desc">
				<ul class="list-sort">
					<li><a href="">Populer</a></li>
					<li><a href="">Rekomendasi</a></li>
					<li class="active"><a href=""><i class="fa fa-check"></i>Terdekat</a></li>
					<li><a href="">Terjauh</a></li>
					<li><a href="">Termurah</a></li>
					<li><a href="">Termahal</a></li>
				</ul>
			</div>
		</div>
		<div class="side-filter" style="display: none" id="id-filter">
			<div class="desc">
				<p class="mt-3">FILTER</p>
				<hr class="divider">
				<p>Kota</p>
				<input class="form" type="text" name="" placeholder="Masukkan nama kota">
				<p class="mt-3">Kategori</p>
				<hr class="divider">
                    <label class="btn-checkbox">
                    	Pantai<input type="checkbox" checked="checked"><span class="checkmark"></span>
					</label>
					<label class="btn-checkbox">
                    	Pegunungan<input type="checkbox" checked="checked"><span class="checkmark"></span>
					</label>
					<label class="btn-checkbox">
                    	Situs<input type="checkbox" checked="checked"><span class="checkmark"></span>
					</label>
					<label class="btn-checkbox">
                    	Museum<input type="checkbox" checked="checked"><span class="checkmark"></span>
					</label>
					<label class="btn-checkbox">
                    	Bangunan<input type="checkbox" checked="checked"><span class="checkmark"></span>
					</label>


				<p  class="mt-4">Radius (km)</p>
				<input type="range" min="1" max="100" step="1" value="50" class="slider" id="myRange" oninput="showVal(this.value)" onchange="showVal(this.value)">

				<input class="form form-clean text-center text-info" type="text" name="" value="10" id="valBox">
				<div ></div>
				<button class="btn btn-outline-success btn-block mt-3">Terapkan</button>
			</div>
			<script type="text/javascript">
				function showVal(newVal){
				  document.getElementById("valBox").value=newVal;
				}
			</script>
		</div>
	
