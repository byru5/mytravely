       

        <!-- JS -->
        <!-- <script src="assets/js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
        <script type="text/javascript" src="assets/css/fancybox/jquery-1.10.1.min.js"></script>
        
        <!-- CAROUSEL -->
        <script type="text/javascript" src="assets/css/owl/docs/assets/owlcarousel/owl.carousel.js"></script>

        <!-- COPY CLIPBOARD -->
        <script src="assets/js/node_modules/clipboard/dist/clipboard.min.js"></script>


        <!-- FANCYBOX -->
        <script type="text/javascript" src="assets/css/fancybox/fancybox.js"></script>
        <!-- Add fancyBox main JS and CSS files -->
        <script type="text/javascript" src="assets/css/fancybox/jquery.fancybox.js?v=2.1.5"></script>
        <link rel="stylesheet" type="text/css" href="assets/css/fancybox/jquery.fancybox.css" media="screen" />
        
        <!-- SEARCH PAGE-->
        <div class="container-search" style="display: none" id="con_search">
            <div class="page-search">
                <div class="top-nav border-bottom">
                    <div class="row align-items-center">
                        <div class="col-auto pr-0">
                            <span id="close-search"><img src="assets/images/svg/back-black.svg" width="25px"></span>
                        </div>
                        <div class="col px-0"><input type="text" name="" autofocus="autofocus" class="form-search" placeholder="Apa yang anda cari?"></div>
                        <div class="col-auto text-right pl-0">
                            <img src="assets/images/svg/search-color.svg" width="25px">
                        </div>
                    </div>
                </div>
                <div class="desc">
                    <span class="btn btn-history"><a href="">wisata bandung</a>&nbsp;&nbsp;|<i class="fa fa-times-circle-o"></i></span>
                    <span class="btn btn-history"><a href="">rumah air di bogor</a>&nbsp;&nbsp;|<i class="fa fa-times-circle-o"></i></span>
                    <span class="btn btn-history"><a href="">puncak gunung </a>&nbsp;&nbsp;|<i class="fa fa-times-circle-o"></i></span>
                    <span class="btn btn-history"><a href="">wisata</a>&nbsp;&nbsp;|<i class="fa fa-times-circle-o"></i></span>
                    <span class="btn btn-history"><a href="">bogor</a>&nbsp;&nbsp;|<i class="fa fa-times-circle-o"></i></span>
                    <span class="btn btn-history"><a href="">puncak gunung semeru</a>&nbsp;&nbsp;|<i class="fa fa-times-circle-o"></i></span>
                </div>

                <hr>
                <div class="desc">
                    <pre>List autocomplete</pre>
                    <a href="">
                        <div class="row circle-list">
                            <div class="col-auto">
                                <div class="circle-img">
                                    <img src="assets/images/uploaded/01.jpg">
                                </div>
                            </div>
                            <div class="col pl-0">
                                <p class="title text-black mb-0 mt-2">Wisata Gunung Bromo</p>
                                <p class="p-desc">Malang, Jawa Timur</p>
                            </div>
                        </div>
                    </a>
                    <a href="">
                        <div class="row circle-list">
                            <div class="col-auto">
                                <div class="circle-img">
                                    <img src="assets/images/uploaded/01.jpg">
                                </div>
                            </div>
                            <div class="col pl-0">
                                <p class="title text-black mb-0 mt-2">Wisata Gunung Bromo</p>
                                <p class="p-desc">Malang, Jawa Timur</p>
                            </div>
                        </div>
                    </a>
                </div>


               <!--  <div class="clear-history">
                    <a href="#" class="btn btn-outline-danger rounded text-center"><i class="fa fa-times"></i>&nbsp;&nbsp;Hapus Riwayat</a>
                </div> -->
            </div>            
        </div>

        <!-- SEARCH -->
        <script type="text/javascript">
            $(document).ready(function(){
                $('#close-search').click(function(){
                    $('#con_search').hide();
                    $( "body" ).removeClass('active');
                })
            });
            $(document).ready(function(){
                $('#open-search').click(function(){
                    $('#con_search').show();
                    $( "body" ).addClass('active');
                })
            });

        </script>


        <!-- TAB -->
        <script type="text/javascript">
            $(document).ready(function(){
                $('#tab-1').click(function(){
                    $('#content-tab-1').show();
                    $('#content-tab-2').hide();
                })
                $('#tab-2').click(function(){
                    $('#content-tab-2').show();
                    $('#content-tab-1').hide();
                });
                $('.tab .col').click(function() {
                    $(this).siblings('.col').removeClass('active');
                    $(this).addClass('active');
                });
                
            })
        </script>


        <!-- BACKGROUND ANIMATION -->
        <!--  <script type="text/javascript">
            function EasyPeasyParallax() {
                var scrollPos = $(document).scrollTop();
                var targetOpacity = 1;
                scrollPos < 480 ? targetOpacity = '0.'+ (scrollPos*1000)/100 : targetOpacity;
                $('.top-nav.float').css({
                    'background-color': 'rgba(0, 0, 0, '+ targetOpacity +')'
                });
                console.log(scrollPos,targetOpacity);
            };

            $(function(){
                $(window).scroll(function() {
                    EasyPeasyParallax();
                });
            });
        </script> -->

        <!-- NAV DETAIL PAGE -->
        <script>
            var header = jQuery(".top-nav.float");
            var headerScroll = "active";
            
            jQuery(document).scroll( function() {
                if(jQuery(this).scrollTop() > 360) {
                        
                    header.addClass(headerScroll);
                }
                else {

                    header.removeClass(headerScroll);             
                }
            });
            
        </script>


        <!-- CAROUSEL -->
        <script type="text/javascript">
            $('.owl-carousel#slide_1').owlCarousel({
                items:1,
                lazyLoad:true,
                loop:false,
                margin:0,
                center:true,
                dotsContainer: '.container-dots#dots_1',
                video:true
            });
            $('.owl-carousel#slide_2').owlCarousel({
                items:1,
                lazyLoad:true,
                loop:false,
                margin:0,
                center:true,
                dotsContainer: '.container-dots#dots_2',
                video:true
            });
        </script>

        <!-- SHARE -->
        <script type="text/javascript">
            new ClipboardJS('#copy_1');

            $( "#copy_1" ).click(function() {
                $( ".copied" ).show();
                $( ".copied" ).fadeOut( 2000);
            });

            $( "#btn-share" ).click(function() {
                $( ".container-share" ).show();
                $( ".container-icon" ).addClass('show');
                $( "body" ).addClass('active');
            });

            $( ".wrapper").click(function() {
                $( ".container-icon" ).removeClass('show');
                $( ".container-share" ).hide();
                $( "body" ).removeClass('active');
            });

        </script>

        <!-- FILTER -->
        <script type="text/javascript">
             $( "#btn-sortby" ).click(function() {
                $( ".side-filter" ).hide();
                $( ".side-sortby" ).toggle();
                $( ".col-filter" ).toggleClass('active');
                $( "#btn-sortby img" ).toggleClass('ico-chevron');
                $( ".col-sortby" ).removeClass('active');
                $( "body" ).toggleClass('active');
                $( "body" ).removeClass('active_2');
               
            });

            $( "#btn-filter" ).click(function() {
                $( ".side-filter" ).toggle();
                $( ".side-sortby" ).hide();
                $( ".col-sortby" ).toggleClass('active');
                $( ".col-filter" ).removeClass('active');
                $( "#btn-sortby img" ).removeClass('ico-chevron');
                $( "body" ).toggleClass('active_2');
                $( "body" ).removeClass('active');
            });
        </script>
    
    </body>
</html>