<!DOCTYPE html>
<html>
	<head>
		<title>Mytravely</title>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    
	    <!-- WHITE --> 
	    <!-- <meta name="theme-color" content="#FFF" /> -->

	    <!-- SOFT GRAY -->
	    <meta name="theme-color" content="#e9ecef" /> 

	    <!-- SOLID BLACK -->
	    <!-- <meta name="theme-color" content="#000" />  -->
	    
	    <!-- MAGENTA -->
	    <!-- <meta name="theme-color" content="#CE0048" /> -->
	    
	    <meta name="mobile-web-app-capable" content="yes">

	    <link rel="icon" href="assets/images/png/icon.png" type="image/png" sizes="16x16">


	    <!-- Styles -->
	    
	    <link rel="stylesheet" href="assets/css/owl/docs/assets/owlcarousel/assets/owl.carousel.css">
    	<link rel="stylesheet" href="assets/css/owl/docs/assets/owlcarousel/assets/owl.theme.default.css">
	    <link rel="stylesheet" type="text/css" href="assets/css/scss/mytravely.css">
	    <link rel="stylesheet" type="text/css" href="assets/css/fonts/font-awesome.min.css">


	</head>

	<body class="">