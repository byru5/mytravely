
<?php include 'layout/head_src.php' ;?>
<?php include 'layout/top_nav.php' ;?>
	
	<div class="container full">
		<div class="desc">
			<div class="vertical-middle text-center">
				<div class="middle">
					<img src="assets/images/svg/page_not_found.svg">
					<h1 class="text-red mt-3">Oops!</h1>
					<h2>Halaman yang Anda tuju tidak <br> dapat ditemukan.</h2>
					<p class="text-gray">Silahkah periksa kembali tautan yang Anda tuju</p>
					<a href="index.php" class="btn btn-success btn-lg mt-3"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali ke Beranda</a>
				</div>
			</div>
		</div>
	</div>
	<div class="full-nav bottom">
		<div class="container">
			<div class="bottom-nav">
				<div class="row">
					<div class="col text-center">
						<a href="index.php">
							<div class="img-home">
								<img src="assets/images/svg/menu-home-no.svg">
							</div>
							<p>beranda</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="category.php">
							<div class="img-category">
								<img src="assets/images/svg/menu-category-no.svg">
							</div>
							<p>kategori</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="nearest-gps.php" class="active">
							<div class="img-near">
								<img src="assets/images/svg/menu-near-no.svg">
							</div>
							<p>tedekat</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="popular.php">
							<div class="img-popular">
								<img src="assets/images/svg/menu-popular-no.svg">
							</div>
							<p>populer</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
		
		
<?php include 'layout/footer.php' ;?>

	
