
<?php include 'layout/head_src.php' ;?>
<?php include 'layout/top_nav.php' ;?>
	
	
	<div class="container full">
		<div class="wrap-nav"></div>
		<div class="desc">
			<div class="row pt-3">
				<div class="col-6 text-center pl-4 pr-3 mb-3">
					<a href="category-sub.php">
						<div class="list-category">
							<img src="assets/images/svg/cat-petualangan.svg">
							<p>Petualangan</p>
						</div>
					</a>
				</div>
				<div class="col-6 text-center pr-4 pl-3 mb-3">
					<a href="category-sub.php">
						<div class="list-category">
							<img src="assets/images/svg/cat-kuliner.svg">
							<p>Kuliner</p>
						</div>
					</a>
				</div>
				<div class="col-6 text-center pl-4 pr-3">
					<a href="category-sub.php">
						<div class="list-category">
							<img src="assets/images/svg/cat-pasangan.svg">
							<p>Pasangan</p>
						</div>
					</a>
				</div>
				<div class="col-6 text-center pr-4 pl-3">
					<a href="category-sub.php">
						<div class="list-category">
							<img src="assets/images/svg/cat-keluarga.svg">
							<p>Keluarga</p>
						</div>
					</a>
				</div>
			</div>
			<br>
			<br>
			<p>Terpopuler saat ini</p>
		</div>
		<hr class="divider my-0">
		<div class="desc">
			<a href="">
				<div class="row circle-list">
					<div class="col-auto">
						<div class="circle-img">
							<img src="assets/images/uploaded/01.jpg">
						</div>
					</div>
					<div class="col px-0">
						<p class="title text-black mb-0	mt-2">Dago, Bandung</p>
						<p class="p-desc">Dago Dream Park, Curug Dago, Tebing Keraton, dll as asd as</p>
					</div>
					<div class="col-auto">
						<div class="btn btn-bagdes mt-3">1</div>
					</div>
				</div>
			</a>
			<a href="">
				<div class="row circle-list">
					<div class="col-auto">
						<div class="circle-img">
							<img src="assets/images/uploaded/01.jpg">
						</div>
					</div>
					<div class="col px-0">
						<p class="title text-black mb-0	mt-2">Puncak Bogor</p>
						<p class="p-desc">Little Venice Puncak, Taman Wisata Alam Gunung Pancar, Gunung Batu Jonggol, dll</p>
					</div>
					<div class="col-auto">
						<div class="btn btn-bagdes mt-3">99+</div>
					</div>
				</div>
			</a>
			<a href="">
				<div class="row circle-list">
					<div class="col-auto">
						<div class="circle-img">
							<img src="assets/images/uploaded/01.jpg">
						</div>
					</div>
					<div class="col px-0">
						<p class="title text-black mb-0	mt-2">Dago, Bandung</p>
						<p class="p-desc">Dago Dream Park, Curug Dago, Tebing Keraton, dll as asd as</p>
					</div>
					<div class="col-auto">
						<div class="btn btn-bagdes mt-3">58</div>
					</div>
				</div>
			</a>
		</div>
	</div>

	<div class="wrap-nav"></div>
	<div class="full-nav bottom">
		<div class="container">
			<div class="bottom-nav">
				<div class="row">
					<div class="col text-center">
						<a href="index.php">
							<div class="img-home">
								<img src="assets/images/svg/menu-home-no.svg">
							</div>
							<p>beranda</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="category.php" class="active">
							<div class="img-category">
								<img src="assets/images/svg/menu-category-no.svg">
							</div>
							<p>kategori</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="nearest-gps.php">
							<div class="img-near">
								<img src="assets/images/svg/menu-near-no.svg">
							</div>
							<p>tedekat</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="popular.php">
							<div class="img-popular">
								<img src="assets/images/svg/menu-popular-no.svg">
							</div>
							<p>populer</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
		
		
<?php include 'layout/footer.php' ;?>

	
