
<?php include 'layout/head_src.php' ;?>
<?php include 'layout/top_nav.php' ;?>
	
	<div class="wrap-nav"></div>

	<div class="container">	
		<?php include 'layout/filter.php' ?>
		<div class="wrap-nav"></div>
		<div class="desc">
			<div class="row">
				<div class="col-6 pr-2">
					<div class="card-2">
						<div class="square">
							<div class="images">
								<img src="assets/images/uploaded/02.jpg" class="object-fit_cover" >
							</div>
						</div>
					</div>
					<div class="card-2-desc">
						<p class="title">Pulau Kakaban</p>
						<p class="sub-title">10 km dari sini</p>
					</div>
				</div>
				<div class="col-6 pl-2">
					<div class="card-2">
						<div class="portrait">
							<div class="images">
								<img src="assets/images/uploaded/02.jpg" class="object-fit_cover" >
							</div>
						</div>
					</div>
					<div class="card-2-desc">
						<p class="title">Pulau Kakaban lorem asd as d</p>
						<p class="sub-title">10 km dari sini</p>
					</div>
				</div>
			</div>
			<hr class="divider">
			<div class="row">
				<div class="col-6 pr-2">
					<div class="card-2">
						<div class="square">
							<div class="images">
								<img src="assets/images/uploaded/02.jpg" class="object-fit_cover" >
							</div>
						</div>
					</div>
					<div class="card-2-desc">
						<p class="title">Pulau Kakaban</p>
						<p class="sub-title">10 km dari sini</p>
					</div>
				</div>
				<div class="col-6 pl-2">
					<div class="card-2">
						<div class="square">
							<div class="images">
								<img src="assets/images/uploaded/02.jpg" class="object-fit_cover" >
							</div>
						</div>
					</div>
					<div class="card-2-desc">
						<p class="title">Pulau Kakaban lorem asd as d</p>
						<p class="sub-title">10 km dari sini</p>
					</div>
				</div>
			</div>
			<hr class="divider">
			<div class="row">
				<div class="col-6 pr-2">
					<div class="card-2">
						<div class="portrait">
							<div class="images">
								<img src="assets/images/uploaded/02.jpg" class="object-fit_cover" >
							</div>
						</div>
					</div>
					<div class="card-2-desc">
						<p class="title">Pulau Kakaban</p>
						<p class="sub-title">10 km dari sini</p>
					</div>
				</div>
				<div class="col-6 pl-2">
					<div class="card-2">
						<div class="square">
							<div class="images">
								<img src="assets/images/uploaded/02.jpg" class="object-fit_cover" >
							</div>
						</div>
					</div>
					<div class="card-2-desc">
						<p class="title">Pulau Kakaban lorem asd as d</p>
						<p class="sub-title">10 km dari sini</p>
					</div>
				</div>
			</div>
			<hr class="divider">
			<div class="row">
				<div class="col-6 pr-2">
					<div class="card-2">
						<div class="portrait">
							<div class="images">
								<img src="assets/images/uploaded/02.jpg" class="object-fit_cover" >
							</div>
						</div>
					</div>
					<div class="card-2-desc">
						<p class="title">Pulau Kakaban</p>
						<p class="sub-title">10 km dari sini</p>
					</div>
				</div>
				<div class="col-6 pl-2">
					<div class="card-2">
						<div class="portrait">
							<div class="images">
								<img src="assets/images/uploaded/02.jpg" class="object-fit_cover" >
							</div>
						</div>
					</div>
					<div class="card-2-desc">
						<p class="title">Pulau Kakaban lorem asd as d</p>
						<p class="sub-title">10 km dari sini</p>
					</div>
				</div>
			</div>
			<hr class="divider">
			<div class="row">
				<div class="col-6 pr-2">
					<div class="card-2">
						<div class="placement p_pt"></div>
					</div>
					<div class="card-2-desc">
						<div class="placement p_2"></div>
						<div class="placement p_3"></div>
					</div>
				</div>
				<div class="col-6 pl-2">
					<div class="card-2">
						<div class="placement p_sq"></div>
					</div>
					<div class="card-2-desc">
						<div class="placement p_2"></div>
						<div class="placement p_3"></div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="stack-page">
		<p>Anda telah mencapai batas halaman terakhir</p>
		<a href="#" class="btn btn-outline-secondary rounded">&nbsp;&nbsp;<i class="fa fa-arrow-up"></i>&nbsp;&nbsp;Kembali ke Atas&nbsp;&nbsp;</a>
	</div>

	<div class="wrap-nav"></div>
	<div class="full-nav bottom" id="id-bottom-nav">
		<div class="container">
			<div class="bottom-nav">
				<div class="row">
					<div class="col text-center">
						<a href="index.php">
							<div class="img-home">
								<img src="assets/images/svg/menu-home-no.svg">
							</div>
							<p>beranda</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="category.php">
							<div class="img-category">
								<img src="assets/images/svg/menu-category-no.svg">
							</div>
							<p>kategori</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="nearest-gps.php" class="active">
							<div class="img-near">
								<img src="assets/images/svg/menu-near-no.svg">
							</div>
							<p>tedekat</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="popular.php">
							<div class="img-popular">
								<img src="assets/images/svg/menu-popular-no.svg">
							</div>
							<p>populer</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
        var prevScrollpos = window.pageYOffset;
        window.onscroll = function() {
            var currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
                document.getElementById("id-top-nav").style.top = "0px";
                // document.getElementById("id-bottom-nav").style.bottom = "0px";
                document.getElementById("id-filter-nav").style.top = "45px";
                document.getElementById("id-sortby").style.top = "90px";
                document.getElementById("id-filter").style.top = "90px";
            } else {
                document.getElementById("id-top-nav").style.top = "-50px";
                // document.getElementById("id-bottom-nav").style.bottom = "-50px";
                document.getElementById("id-filter-nav").style.top = "0px";
                document.getElementById("id-sortby").style.top = "45px";
                document.getElementById("id-filter").style.top = "45px";
            }
            prevScrollpos = currentScrollPos;
        }
    </script>
	
	
<?php include 'layout/footer.php' ;?>

	
