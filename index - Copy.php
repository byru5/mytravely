
<?php include 'layout/head_src.php' ;?>
<?php include 'layout/top_nav.php' ;?>
	
	<div class="wrap-nav mb-3"></div>

	<div class="container">
		<div class="card">
			<div class="badges popular">populer</div>
			<div class="card-1">
				<div class="square">
					<div class="images">
						<div class="owl-carousel owl-theme" id="slide_1">
						    <div class="object-fit_cover item-video" data-merge="1"><a class="owl-video" href="https://www.youtube.com/watch?v=JpxsRwnRwCQ"></a></div>
						    <img class="object-fit_cover owl-lazy" data-src="assets/images/uploaded/02.jpg" alt="">
						    <img class="object-fit_cover owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						    <img class="object-fit_cover owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						    <img class="object-fit_cover owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						</div>
					</div>
				</div>
				<div class="owl-theme">
					<div class="owl-nav">
						<div class="container-dots" id="dots_1"></div>
					</div>
				</div>
			</div>
			<a href="index-detail.php" >
				<div class="desc">
					<div class="row">
						<div class="col">
							<p class="title text-black text-black">Wisata Gunung Bromo</p>
							<p class="sub-title text-secondary">Malang, Jawa Timur</p>
						</div>
						<div class="col-auto">
							<p class="stars text-right text-warning">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-o"></i>
								&nbsp;8.5
							</p>
							<p class="sub-title text-right text-secondary">
								355 ulasan
							</p>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="card">
			<div class="badges recom">rekomendasi</div>
			<div class="card-1">
				<div class="square">
					<div class="images">
						<div class="owl-carousel owl-theme" id="slide_2">
						    <img class="owl-lazy" data-src="assets/images/uploaded/02.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						</div>
					</div>
				</div>
				<div class="owl-theme">
					<div class="owl-nav">
						<div class="container-dots" id="dots_2"></div>
					</div>
				</div>
			</div>
			<a href="index-detail.php">
				<div class="desc">
					<div class="row">
						<div class="col">
							<p class="title text-black">Wisata Gunung Bromo</p>
							<p class="sub-title text-secondary">Malang, Jawa Timur</p>
						</div>
						<div class="col-auto">
							<p class="stars text-right text-warning">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-o"></i>
								&nbsp;8.5
							</p>
							<p class="sub-title text-right text-secondary">
								355 ulasan
							</p>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="card">
			<div class="badges near">terdekat</div>
			<div class="card-1">
				<div class="square">
					<div class="images">
						<div class="owl-carousel owl-theme">
						    <img class="owl-lazy" data-src="assets/images/uploaded/02.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						</div>
					</div>
				</div>
				<div class="owl-theme">
					<div class="owl-nav">
						<div class="container-dots"></div>
					</div>
				</div>
			</div>
			<a href="index-detail.php">
				<div class="desc">
					<div class="row">
						<div class="col">
							<p class="title text-black">Wisata Gunung Bromo Lorem ipsum dolor sit amet, consectetur adip isicing elit</p>
							<p class="sub-title text-secondary">Malang, Jawa Timur</p>
						</div>
						<div class="col-auto">
							<p class="stars text-right text-warning">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-o"></i>
								&nbsp;8.5
							</p>
							<p class="sub-title text-right text-secondary">
								355 ulasan
							</p>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="card">
			<div class="badges near">terdekat</div>
			<div class="card-1">
				<div class="square">
					<div class="images">
						<div class="owl-carousel owl-theme">
						    <img class="owl-lazy" data-src="assets/images/uploaded/02.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						</div>
					</div>
				</div>
				<div class="owl-theme">
					<div class="owl-nav">
						<div class="container-dots"></div>
					</div>
				</div>
			</div>
			<a href="index-detail.php">
				<div class="desc">
					<div class="row">
						<div class="col">
							<p class="title text-black">Wisata Gunung Bromo Lorem ipsum dolor sit amet, consectetur adip isicing elit</p>
							<p class="sub-title text-secondary">Malang, Jawa Timur</p>
						</div>
						<div class="col-auto">
							<p class="stars text-right text-warning">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-o"></i>
								&nbsp;8.5
							</p>
							<p class="sub-title text-right text-secondary">
								355 ulasan
							</p>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="card">
			<div class="badges near">terdekat</div>
			<div class="card-1">
				<div class="square">
					<div class="images">
						<div class="owl-carousel owl-theme">
						    <img class="owl-lazy" data-src="assets/images/uploaded/02.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						</div>
					</div>
				</div>
				<div class="owl-theme">
					<div class="owl-nav">
						<div class="container-dots"></div>
					</div>
				</div>
			</div>
			<a href="index-detail.php">
				<div class="desc">
					<div class="row">
						<div class="col">
							<p class="title text-black">Wisata Gunung Bromo Lorem ipsum dolor sit amet, consectetur adip isicing elit</p>
							<p class="sub-title text-secondary">Malang, Jawa Timur</p>
						</div>
						<div class="col-auto">
							<p class="stars text-right text-warning">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-o"></i>
								&nbsp;8.5
							</p>
							<p class="sub-title text-right text-secondary">
								355 ulasan
							</p>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="card">
			<div class="badges near">terdekat</div>
			<div class="card-1">
				<div class="square">
					<div class="images">
						<div class="owl-carousel owl-theme">
						    <img class="owl-lazy" data-src="assets/images/uploaded/02.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						    <img class="owl-lazy" data-src="assets/images/uploaded/01.jpg" alt="">
						</div>
					</div>
				</div>
				<div class="owl-theme">
					<div class="owl-nav">
						<div class="container-dots"></div>
					</div>
				</div>
			</div>
			<a href="index-detail.php">
				<div class="desc">
					<div class="row">
						<div class="col">
							<p class="title text-black">Wisata Gunung Bromo Lorem ipsum dolor sit amet, consectetur adip isicing elit</p>
							<p class="sub-title text-secondary">Malang, Jawa Timur</p>
						</div>
						<div class="col-auto">
							<p class="stars text-right text-warning">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-o"></i>
								&nbsp;8.5
							</p>
							<p class="sub-title text-right text-secondary">
								355 ulasan
							</p>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	
	<div class="fr-spinner">
		<div class="sk-fading-circle">
		  <div class="sk-circle1 sk-circle"></div>
		  <div class="sk-circle2 sk-circle"></div>
		  <div class="sk-circle3 sk-circle"></div>
		  <div class="sk-circle4 sk-circle"></div>
		  <div class="sk-circle5 sk-circle"></div>
		  <div class="sk-circle6 sk-circle"></div>
		  <div class="sk-circle7 sk-circle"></div>
		  <div class="sk-circle8 sk-circle"></div>
		  <div class="sk-circle9 sk-circle"></div>
		  <div class="sk-circle10 sk-circle"></div>
		  <div class="sk-circle11 sk-circle"></div>
		  <div class="sk-circle12 sk-circle"></div>
		</div>
	</div>


	<div class="wrap-nav"></div>
	<div class="full-nav bottom">
		<div class="container">
			<div class="bottom-nav">
				<div class="row">
					<div class="col text-center">
						<a href="index.php" class="active">
							<div class="img-home">
								<img src="assets/images/svg/menu-home-no.svg">
							</div>
							<p>beranda</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="category.php">
							<div class="img-category">
								<img src="assets/images/svg/menu-category-no.svg">
							</div>
							<p>kategori</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="nearest-gps.php">
							<div class="img-near">
								<img src="assets/images/svg/menu-near-no.svg">
							</div>
							<p>tedekat</p>
						</a>
					</div>
					<div class="col text-center">
						<a href="popular.php">
							<div class="img-popular">
								<img src="assets/images/svg/menu-popular-no.svg">
							</div>
							<p>populer</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
		
		
		
		
		
		
		

	
	
	
		

		
		
<?php include 'layout/footer.php' ;?>

	
